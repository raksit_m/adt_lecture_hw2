/**
 * Program which calculate n^2 when n is a natural number.
 * 
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class PerfectSquare {

	public static int square(int n) {
		
		if(n == 1) return 1;
		
		else return square(n - 1) + 2*(n - 1) + 1;
	}
}
